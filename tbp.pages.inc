<?php

/**
 * Translate $perms from grant $defs
 *
 * @param $perm
 *   Index of grants, commonly from tbp_user_permissions_form.
 * @param $defs
 *   Grants list.
 *
 * @return
 *   Array with grants traversed from grants index($perms).
 */
function _tbp_perms_from_def($perms, $defs) {
  $return = array();
  foreach ($perms as $index) {
    $return[] = $defs[$index];
  }
  return $return;
}

/**
 * Determine if given user ID is a TBP admin.
 *
 * @return
 *   TRUE: if given user ID has permissions 'administer Taxonomy Based
 *         Permissions'.
 */
function _tbp_is_admin($user_id) {
  return user_access(
    'administer Taxonomy Based Permissions',
    user_load(array('uid' => $user_id))
  );
}

/**
 * Permissions legend
 */
function tbp_permissions_legend() {
  $form['legend'] = array(
    '#type' => 'item',
    '#value' => t('<h3>!title</h3>
!view: user can VIEW content <b>in</b> desired term.<br />
!create: user can CREATE content <b>for</b> desired term.<br />
!update: user can UPDATE content <b>in</b> desired term.<br />
!delete: user can DELETE content <b>in</b> desired term.<br />
!delegate: user can DELEGATE his permissions <b>to</b> other users</b>.<br />
!inherit: user can INHERIT the <i>delegate permission</i> <b>to</b> other users</b>.',
      array(
        '!title' => t('Legend'),
        '!view' => t('view'),
        '!create' => t('create'),
        '!update' => t('update'),
        '!delete' => t('delete'),
        '!delegate' => t('delegate'),
        '!inherit' => t('inherit'),
      )
    ),
  );
  return drupal_render($form);
}

/**
 * Permissions form for users on categories.
 */
function tbp_add_user_form($term) {
  // Category data
  $form['vid'] = array('#type' => 'hidden', '#default_value' => $term->vid);
  $form['tid'] = array('#type' => 'hidden', '#default_value' => $term->tid);

  $form['user_id'] = array(
    '#type' => 'textfield',
    '#description' => t('Lookup a username and add it to the list below.'),
    '#autocomplete_path' => 'node/'. arg(1) .'/tbp_username_helper_autocomplete',
    '#size' => 40,
    '#maxlength' => 60,
    '#weight' => -2,
    '#access' => TRUE,
  );
  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Add user'),
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Validate handler for tbp_add_user_form().
 */
function tbp_add_user_form_validate($form_id, $form_values, $form) {
  // Read $user_id
  $split = strpos($form_values['user_id'], ' ');
  $user_id = (int)($split !== FALSE ?
    substr($form_values['user_id'], 0, $split) :
    $form_values['user_id']
  );

  // Look for $user_id
  $rs = db_result(db_query('SELECT uid FROM {users} WHERE uid = %d AND uid != 1', $user_id));

  if ($rs) {
    form_set_value($form['user_id'], $user_id);
  }
  else {
    form_set_error('user_id', t('No such user %user', array('%user' => $form_values['user_id'])));
  }
}

/**
 * Submit handler for tbp_add_user_form().
 */
function tbp_add_user_form_submit($form_id, $form_values) {
  if (
    $form_values['op'] == t('Add user') &&
    ($user_id = $form_values['user_id']) > 1
  ) {
    // Get permissions for given user
    $perms = tbp_get_setting('user_'. $user_id);
    // Check if user is already there(prevent overriding)
    if (!isset($perms[$form_values['vid']][$form_values['tid']])) {
      $perms[$form_values['vid']][$form_values['tid']] = array();
      // Save permissions
      tbp_set_setting('user_'. $user_id, $perms);
    }

    // Keep sync with index of users by term
    $term_users = tbp_get_setting('term_users');
    if (!isset($term_users[$form_values['tid']][$user_id])) {
      $term_users[$form_values['tid']][$user_id] = $user_id;
      // Save index
      tbp_set_setting('term_users', $term_users);
    }
  }
}

/**
 * Display permission crontrols by given params.
 *
 * @param $user_id
 *   Must be a valid User ID.
 * @param $term
 *   Must be a valid term object.
 * @param $perm
 *   Permissions available for displaying. Should look like:
 *   array(
 *     'type' => array(// node type
 *       0 => 1 // view permission
 *       1 => 2 // create permission
 *       ...
 *     )
 *   );
 * @param $node_type
 *   (Optional)
 *   Display permission controls for such $node_type only.
 */
function tbp_user_permissions_form($user_id, $term, $perm, $node_type = NULL) {
  drupal_add_css(drupal_get_path('module', 'tbp') .'/tbp.css');

  $grants = tbp_get_setting('user_grants');
  $tbp_grants = tbp_get_setting('grants');
  $permissions = tbp_get_setting('user_'. $user_id);
  $types = node_get_types();
  $managed_types = tbp_get_setting('managed_types');
  // Filter $managed_types
  if (!is_null($node_type)) {
    $managed_types = array($term->vid => array($node_type));
  }
  $form = array('#tree' => TRUE);

  $form['definitions'] = array(
    '#type' => 'value',
    '#value' => serialize($grants),
  );
  $form['scope_user_id'] = array(
    '#type' => 'value',
    '#value' => $perm->user_id,
  );

  // Grants for content types
  foreach ($managed_types[$term->vid] as $type) {
    if (isset($perm->perm[$type]) ? in_array(5, $perm->perm[$type]) : FALSE) {
      $form['user'][$user_id][$term->vid][$term->tid][$type] = array(
        '#type' => 'fieldset',
        '#title' => $types[$type]->name,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $count = 0; // For controlling rows
      foreach ($grants as $index => $grant) {
        ++$count;
        if ($grant == 5 && !in_array(6, $perm->perm[$type])) {
          break;
        }
        if (in_array($grant, $perm->perm[$type])) {
          $form['user'][$user_id][$term->vid][$term->tid][$type][$index] = array(
            '#type' => 'checkbox',
            '#title' => in_array($grant, array_keys($tbp_grants)) ?
              $tbp_grants[$grant]->name : $grant,
            '#default_value' =>
              isset($permissions[$term->vid][$term->tid][$type]) ?
                in_array($grant, $permissions[$term->vid][$term->tid][$type]) :
                FALSE,
          );
        }
      }
    }
  }
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['remove'] = array(
    '#type' => 'submit',
    '#value' => t('Remove'),
  );
  // Let drupal to process multiple instances of this form on same page
  $form['#submit'] = array('tbp_user_permissions_form_submit' => 1);

  return $form;
}

/**
 * Implementation of hook_submit().
 */
function tbp_user_permissions_form_submit($form_id, $form_values) {
  global $user; $_user =& $user;

  $op = $form_values['op'];
  if (in_array($op, array(t('Remove'), t('Save')))) {
    $reset_menu = FALSE;
    // Scope grants
    $scope_user_id = $form_values['scope_user_id'];
    $admin = _tbp_is_admin($scope_user_id);
    if ($admin) {
      $scope_grants = array_shift(tbp_get_setting('default_perm'));
    }
    else {
      $grants = tbp_get_setting('user_'. $scope_user_id);
    }
    // Traverse permissions from $form_values
    $definitions = unserialize($form_values['definitions']);
    $permissions = array();
    foreach ($form_values['user'] as $user_id => $vids) {
      $user_grants = tbp_get_setting('user_'. $user_id);
      foreach ($vids as $vid => $terms) {
        foreach ($terms as $tid => $types) {
          foreach ($types as $type => $perms_index) {
            // Determine $scope_grants
            if (!$admin) {
              if (isset($grants[$vid][$tid][$type])) {
                $scope_grants =& $grants[$vid][$tid][$type];
              }
              else {
                $scope_grants = array(); // This is a weird case
              }
            }
            if (count($perms_index)) {
              if ($op == t('Save')) {
                // Translate $perms_index index to $permissions from grants
                $permissions['add'] = _tbp_perms_from_def(
                  array_keys(preg_grep('/^1$/', $perms_index)), $definitions
                );
                $permissions['rem'] = _tbp_perms_from_def(
                  array_keys(preg_grep('/^0$/', $perms_index)), $definitions
                );
              }
              elseif ($op == t('Remove')) {
                $permissions['add'] = array();
                $permissions['rem'] = _tbp_perms_from_def(
                  array_keys($perms_index), $definitions
                );
              }
              // SCOPE VARIABLES
              // $permissions: those indented to be added/removed to user
              // $scope_grants: permissions able to be granted
              // $user_grants: current permissions of user
              // Conciliation, set effective permissions
              foreach ($permissions as $action => $perms) {
                foreach ($perms as $perm) {
                  if (in_array($perm, $scope_grants)) {
                  // $perm is able to be assigned/unassigned
                    // Validate tree
                    if (!isset($user_grants[$vid][$tid][$type])) {
                      $user_grants[$vid][$tid][$type] = array();
                    }
                    // Look for permission
                    $found = array_search($perm, $user_grants[$vid][$tid][$type]);
                    if ($action == 'add') {
                      // Add permission if not $found
                      if ($found === FALSE) {
                        $user_grants[$vid][$tid][$type][] = $perm;
                      }
                    }
                    elseif ($action == 'rem') {
                      // Remove permission if $found
                      if ($found !== FALSE) {
                        unset($user_grants[$vid][$tid][$type][$found]);
                      }
                    }
                  }
                }
              }
            }
            // Clean-up empty types
            if (
              isset($user_grants[$vid][$tid][$type]) &&
              !count($user_grants[$vid][$tid][$type])
            ) {
              unset($user_grants[$vid][$tid][$type]);
            }
          }
          // Clean-up empty terms
          if (
            isset($user_grants[$vid][$tid]) && !count($user_grants[$vid][$tid])
          ) {
            unset($user_grants[$vid][$tid]);
          }
        }
        // Clean-up empty vocabularies
        if (
          isset($user_grants[$vid]) &&
          (
            // TODO: Let not only superuser but privileged roles to remove
            // user from tbp permissions.
            !count($user_grants[$vid]) ||
            ($user->uid == 1 && $op == t('Remove'))
          )
        ) {
          unset($user_grants[$vid]);
        }
      }
      // Remove
      if (!count($user_grants) && $op == t('Remove')) {
        // Clean-up empty user's permission variables
        variable_del('tbp_user_'. $user_id);
        // Clean-up index of users by term
        $term_users = tbp_get_setting('term_users');
        if (isset($term_users[$tid][$user_id])) {
          unset($term_users[$tid][$user_id]);
          tbp_set_setting('term_users', $term_users);
        }
      }
      elseif ($op == t('Remove')) {
        drupal_set_message('You have not enough grants to remove the user');
      }

      // Save
      if ($op == t('Save')) {
        $reset_menu = TRUE;
        tbp_set_setting('user_'. $user_id, $user_grants);
      }
    }
    if ($reset_menu) {
      // Clear menu cache in order to let tbp.module override node/add/* paths
      cache_clear_all('*', 'cache_menu', TRUE);
    }
  }
}

/**
 * Display user permissions.
 */
function tbp_user_permissions_overview($user_id) {
  $form = array('#tree' => TRUE);
  $form['user_id'] = array(
    '#type' => 'value',
    '#value' => $user_id,
  );
  $form['legend'] = array(
    '#type' => 'item',
    '#value' => tbp_permissions_legend(),
  );

  // Need to get lots of settings
  $managed_vocab = tbp_get_setting('managed_vocab');
  $vocabularies = tbp_get_setting('vocabularies');
  // Retrieve $vids and $terms for later rendering
  $vids = array(); $terms = array();
  foreach ($managed_vocab as $vid) {
    $vids[$vid] = $vocabularies[$vid]->name;
    $tree = taxonomy_get_tree($vid);
    foreach ($tree as $term)
      $terms[$vid][$term->tid] = $term->name;
  }
  $grants = tbp_get_setting('grants_array');
  $permissions = tbp_get_setting('user_'. $user_id);
  $managed_types = tbp_get_setting('managed_types');
  $content_types = node_get_types(); // content type labels
  drupal_add_css(drupal_get_path('module', 'tbp') .'/tbp.css');

  // Obfuscated translate function
  $t = 't';

  // Categories
  foreach ($permissions as $vid => $terms) {
    $form['vids'][$vid] = array(
      '#type' => 'fieldset',
      '#title' => $vocabularies[$vid]->name .' '.
                  t('(%taxonomy)', array('%taxonomy' => t('taxonomy'))),
      '#tree' => TRUE,
      '#collapsible' => FALSE,
    );
    // Terms
    foreach ($terms as $tid => $types) {
      $term = taxonomy_get_term($tid);
      $form['vids'][$vid][$tid] = array(
        '#type' => 'fieldset',
        '#title' => $term->name .' '.
                    t('(%term)', array('%term' => t('term'))),
        '#tree' => TRUE,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      // Content types
      foreach ($types as $type => $perm) {
        if (in_array($type, $managed_types[$vid])) {
          $form['vids'][$vid][$tid][$type] = array(
            '#type' => 'fieldset',
            '#title' => $content_types[$type]->name .' '.
                        t('(%content_type)', array('%content_type' => t('content type'))),
            '#tree' => TRUE,
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
          );
          // Grants
          foreach ($perm as $grant) {
            if (isset($grants[$grant])) {
              $form['vids'][$vid][$tid][$type][$grant] = array(
                '#type' => 'item',
                '#value' => $t($grants[$grant]),
              );
            }
          }
        }
      }
    }
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Applicable TBP permissions page by given node ID type and user ID.
 * 
 * @param $op
 *   Displays permission controls when by following possible values:
 *     'node': node type and its terms
 *     'term': term and its content types
 *
 * @param $id
 *   ID to load an object. Depends on $op.
 *
 * @param $user_id
 *   A valid user ID, SHOULD be Integer 
 */
function tbp_permissions_page($op, $id, $user_id) {
  drupal_add_css(drupal_get_path('module', 'tbp') .'/tbp.css');

  $managed_vocab = tbp_get_setting('managed_vocab');
  $term_users = tbp_get_setting('term_users');
  $output = tbp_permissions_legend();
  $allowed_term = array();

  switch ($op) {
    case 'node':
      $node = node_load($id);
      $types = node_get_types();
      drupal_set_title(t('Permissions for content type @type', array('@type' => $types[$node->type]->name)));
      break;
    case 'term':
      $term = taxonomy_get_term($id);
      $node = new stdClass();
      $node->taxonomy = array($term->tid => $term);
      drupal_set_title(t('Permissions for taxonomy term !term', array('!term' => $term->name)));
      break;
  }

  // Render permissions of managed categories associated to given node ID
  foreach ($managed_vocab as $vid) {
    foreach ($node->taxonomy as $term) {
      // Allow terms from vocabularies associated to $node
      if ($term->vid == $vid) {
        $allowed_term[] = $term;
      }
    }
  }

  // Determine which grants user have at this $node
  // TODO: Check if this is secure
  $default_perm = array();
  $perm = array();
  if (!($admin = _tbp_is_admin($user_id))) {
    // Look for delegation grants
    // $node->id is NULL when $op equals to 'term'
    if (_tbp_granted_delegation($user_id, $node->nid)) {
      $perm = tbp_get_setting('user_'. $user_id);
    }
    else {
      return t('You have not delegation rights');
    }
  }
  else {
    // Grant admin with all permissions
    $default_perm = tbp_get_setting('default_perm');
  }

  // Show permission controls for users with delegation grants in $allowed_term
  foreach ($allowed_term as $term) {
    if (
      in_array($term->vid, $managed_vocab) &&
      // User have access rights
      ($admin || isset($perm[$term->vid]))
    ) {
      $term_form = array(
        '#title' => $term->name,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#value' => drupal_get_form('tbp_add_user_form', $term),
      );
      if (
        isset($term_users[$term->tid]) &&
        // If not $admin user can delegate his $perm-issions only
        // NOTICE: Delegation rights were already validated and are double
        //         checked later on tbp_user_permissions_form
        ($admin || $perm[$term->vid][$term->tid])
      ) {
        // List users
        $users = tbp_list_users($term_users[$term->tid], 'in', 'user_id', 'username');
        foreach ($term_users[$term->tid] as $term_user_id) {
          if ((int)$user_id !== $term_user_id) {
            $permissions = (object)array(
              // $user_id is the scope user
              'user_id' => $user_id,
              'perm' => $admin ? $default_perm : $perm[$term->vid][$term->tid],
            );
            $user_form = array(
              '#title' => $users[$term_user_id],
              '#collapsible' => TRUE,
              '#collapsed' => FALSE,
              '#value' => drupal_get_form(
                // $node->type is NULL when $op equals to 'term'
                'tbp_user_permissions_form_'. $term_user_id,
                $term_user_id, $term, $permissions, $node->type),
            );
            $term_form['#value'] .= theme('fieldset', $user_form);
          }
        }
      }
      $output .= theme('fieldset', $term_form);
    }
  }

  return $output;
}

/**
 * Retrieve a list of users matching given criterias.
 *
 * If Nodeprofile modules is enabled, query will try to match in CCK fields.
 * specified by TBP settings, instead of users table.
 * 
 * @param $options
 *   Possible values:
 *     (string): when $condition equals to 'like'
 *     (array) : when $condition equals to 'in'
 * 
 * @param $condition
 *   Possible values:
 *     'like': sub-string of "name" (they Keyword, see Keywords under param $value)
 *             param $options should be string.
 *     'in'  : param $options should be an array containing a list of user IDs, matchs
 *             with {users}.uid
 *
 * @param $key
 *   Possible values:
 *     'value': same as what $value generates
 *     'uid'  : returns {users}.uid
 *     'name' : If Nodeprofile enabled: concat(field1, ' ', field2, ' ', ...),
 *              otherwhise returns {users}.name
 *
 * @param $value
 *   Keywords:
 *     'user_id' : returns {users}.uid
 *     'name'    : If Nodeprofile enabled: concat(field1, ' ', field2, ' ', ...),
 *                 otherwhise return {users}.name
 *     'username': {users}.name
 *   Combinations:
 *     'user_id+name': {users}.uid .' '. name (default)
 *     'name+username': name .' '. {users}.name
 */
function tbp_list_users($options = '', $condition = 'like', $key = 'value', $value = 'user_id+name') {
  if (is_array($options) && !count($options)) {
    return array();
  }
  $matches = array();
  $fields = array();

  $np_type = tbp_get_setting('np_type');
  $np_fields = tbp_get_setting('np_fields');
  if ((module_exists('nodeprofile') && $np_type !== '')) {
    $join = array('{node} n ON n.uid = u.uid' => TRUE);
    // Extract database structure info
    $type = content_types($np_type);
    foreach ($np_fields as $field_name) {
      $field = content_database_info($type['fields'][$field_name]);

      $table = '{'. $field['table'] .'}';
      $join[$table .' ON n.nid = {'. $field['table'] .'}.nid'] = TRUE;
      $fields[$table .'.'. $field['columns']['value']['column']] = TRUE;
    }
  }
  else {
    $fields['u.name'] = TRUE;
  }
  // Fields
  $fields = 'concat('. implode(', " ",', array_keys($fields)) .')';
  // JOIN
  $join = count($join) ? 'JOIN '. implode(' JOIN ', array_keys($join)) .' ' : ' ';
  // WHERE
  switch ($condition) {
    case 'like':
      $conditions = 'LOWER('. $fields .') LIKE LOWER("%%%s%%")';
      break;
    case 'in':
      $conditions = 'u.uid IN('. str_repeat('%d,', count($options) -1) .'%d)';
      break;
  }
  $where = 'WHERE (u.status = 1 AND u.uid != 1) AND '. $conditions;
  // Construct query
  $query = 'SELECT u.uid user_id, u.name username, '. $fields .' name FROM {users} u '. $join . $where;
  // List matches
  $rs = db_query_range($query, $options, 0, 10);
  while ($row = db_fetch_object($rs)) {
    if ($value == 'user_id+name') {
      $match = $row->user_id .' - '. check_plain($row->name);
    }
    elseif ($value == 'username') {
      $match = check_plain($row->name) .' ('. $row->username .')';
    }
    else {
      $match = check_plain($row->$value);
    }
    if ($key == 'value') {
      $matches[$match] = $match;
    }
    else {
      $matches[$row->$key] = $match;
    }
  }
  return $matches;
}

/**
 * Retrieve a pipe delimited string of autocomplete suggestions for existing users.
 */
function tbp_user_autocomplete($string = '') {
  print drupal_to_js(tbp_list_users($string));
  exit();
}
