<?php

/**
 * Implementation of hook_tbp_menu().
 */
function rsvp_tbp_grants() {
  $perms = array('administer rsvp', 'rsvp on events', 'rsvp on own events');
  $grants = array();
  foreach ($perms as $grant) {
    $grants[$grant] = (object)array(
      'id' => $grant, 'name' => t($grant), 'description' => '',
    );
  }
  return $grants;
}

/**
 * Implementation of hook_tbp_menu().
 */
function rsvp_tbp_menu($may_cache) {
  global $user;

  $items = array();

  if (!$may_cache) {
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $nid = arg(1);
      $node = node_load($nid);
      $access = (
        user_access('administer rsvp') || tbp_access('administer rsvp', $node) ||
        user_access('rsvp on events') || tbp_access('rsvp on events', $node) ||
        ($node->uid == $user->uid && (
          user_access('rsvp on own events') || tbp_access('rsvp on own events', $node))));
      $items[] = array(
        'path' => 'node/'. $nid .'/rsvp',
        'title' => t('RSVP'),
        'access' => $access,
        'callback' => 'tbp_rsvp_manage',
        'callback arguments' => $nid,
      );
    }
  }
  return $items;
}

/**
 * Overrides rsvp_manage().
 */
function tbp_rsvp_manage($nid = NULL) {
  $header = array(
    array('data' => t('Title'), 'field' => 'name', 'sort' => 'asc', 'width' => '30%'),
    t('Invites'),
    t('Attending'),
    t('Not attending'),
    t('Might attend'),
    t('No response'),
    array('data' => t('Operations'), 'colspan' => 3),
  );

  $admin_access = user_access('administer rsvp') || tbp_access('administer rsvp', $node);

  if ($nid) {
    global $user;
    $node = node_load($nid);
    drupal_set_title($node->title);
    if (
      $admin_access ||
      user_access('rsvp on events') || tbp_access('rsvp on events', $node) || (
        (user_access('rsvp on own events') || tbp_access('rsvp on own events', $node)) && $node->uid == $user->uid)) {
      if ($node->event_end >= time()) {
        $content = l(t('Create RSVP'), 'node/'. $nid .'/rsvp/create');
      }
      else {
        $content = t('This event has expired, invitations can only be sent to events <br />that have not expired yet.');
      }
    }
  }
  else {
    $header = array_merge(array(array('data' => t('Event'), 'field' => 'title')), $header);
  }

  $rows = array();
  $rsvps = _rsvp_get_rsvps($nid, $admin_access ? FALSE : NULL, tablesort_sql($header));

  if (db_num_rows($rsvps)) {
    while ($rsvp = db_fetch_object($rsvps)) {
      $row = array();
      is_null($nid) ? $row[] = l($rsvp->title, 'node/'. $rsvp->nid, array('title' => 'View event')) : '';
      $row[] = l($rsvp->name, 'node/'. $rsvp->nid .'/rsvp/'. $rsvp->rid .'/view', array('title' => 'View RSVP'));
      // add statistics
      $row[] = db_num_rows(_rsvp_get_attendees($rsvp->rid));
      $row[] = db_num_rows(_rsvp_get_attendees($rsvp->rid, 'yes'));
      $row[] = db_num_rows(_rsvp_get_attendees($rsvp->rid, 'no'));
      $row[] = db_num_rows(_rsvp_get_attendees($rsvp->rid, 'maybe'));
      $row[] = db_num_rows(_rsvp_get_attendees($rsvp->rid, 'none'));
      $row[] = l(t('Edit'), 'node/'. $rsvp->nid .'/rsvp/'. $rsvp->rid .'/edit', array('title' => 'Edit RSVP'));
      $row[] = l(t('Invite'), 'node/'. $rsvp->nid .'/rsvp/'. $rsvp->rid .'/attendees', array('title' => 'Invite more people to attend your rsvp'));
      $row[] = l(t('Send a message'), 'node/'. $rsvp->nid .'/rsvp/'. $rsvp->rid .'/message', array('title' => 'Send people you invited a message'));
      $rows[] = $row;
    }
    $content .= theme('table', $header, $rows);
  }

  if (!$content) {
    // no RSVPs found for the requested user
    $content = t('You don\'t have any RSVPs.');
  }
  return $content;
}
