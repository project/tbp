<?php

/**
 * System settings form.
 */
function tbp_settings_form() {
  $form = array();
/* TODO: Think on some useful text to be placed here.
  $form[] = array(
    '#type' => 'item',
    '#value' => '',
  );
 */

  // Need to store as cache
  $form['grants']['definitions'] = array(
    '#type' => 'value',
    '#value' => serialize($grants),
  );

  // Managed categories
  $form['vids'] = array(
    '#type' => 'fieldset',
    '#title' => t('Managed categories'),
    '#description' => t('Choose categories you wish to be managed by TBP.'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $vids = taxonomy_get_vocabularies();
  $managed_vocab = tbp_get_setting('managed_vocab');
  foreach ($vids as $vid => $taxonomy) {
    $form['vids'][$vid] = array(
      '#type' => 'checkbox',
      '#title' => $taxonomy->name,
      '#default_value' => in_array($vid, $managed_vocab),
    );
  }

  // Allowed Add-ons by Category
  $addons = module_invoke_all('tbp_addon');
  $allowed_addons = tbp_get_setting('allowed_addons');
  $form['addons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Allowed add-ons'),
    '#description' => t('Choose which add-ons are going to be available for all managed categories.'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  if (count($managed_vocab) > 0) {
    foreach ($addons as $addon => $details) {
      $form['addons'][$addon] = array(
        '#type' => 'checkbox',
        '#title' => $details['name'],
        '#default_value' => isset($allowed_addons) ?
          in_array($addon, $allowed_addons) : FALSE,
      );
    }
  }
  else {
    $form['addons'][] = array(
      '#type' => 'item',
      '#value' => t('System should manage at least one category in order to define managed types.'),
    );
  }

  // Managed Content types by Category
  $types = node_get_types();
  $managed_types = tbp_get_setting('managed_types');
  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Managed content types'),
    '#description' => t('Choose which content types are going to be managed by TBP for managed categories.'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  if (count($managed_vocab) > 0) {
    foreach ($managed_vocab as $vid) {
      $form['types'][$vid][] = array(
        '#type' => 'item',
        '#value' => '<b>'. $vids[$vid]->name .'</b>',
      );
      foreach ($types as $type) {
        $form['types'][$vid][$type->type] = array(
          '#type' => 'checkbox',
          '#title' => $type->name,
          '#default_value' => isset($managed_types[$vid]) ?
            in_array($type->type, $managed_types[$vid]) : FALSE,
        );
      }
    }
  }
  else {
    $form['types'][] = array(
      '#type' => 'item',
      '#value' => t('System should manage at least one category in order to define managed types.'),
    );
  }

  if (module_exists('nodeprofile')) {
    // Integration with Nodeprofile
    $types = nodeprofile_get_types();
    $np_type = tbp_get_setting('np_type');
    $form['np'] = array(
      '#type' => 'fieldset',
      '#title' => t('Integrate with Nodeprofile'),
      '#description' => t('Choose which data use instead of default user account.'),
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    if (count($types) > 0) {
      $options[''] = t('- None selected -');
      foreach ($types as $type) {
        $options[$type->type] = $type->name;
      }
      $form['np']['np_type'] = array(
        '#type' => 'select',
        '#title' => t('Content type'),
        '#description' => t('Select a content type marked as nodeprofile.'),
        '#options' => $options,
        '#default_value' => $np_type,
      );
      if ($np_type != '') {
        $options = array();
        $fields = _content_type_info();
        foreach ($fields['content types'][$np_type]['fields'] as $field) {
          $options[$field['field_name']] = $field['widget']['label'];
        }
        $np_fields = tbp_get_setting('np_fields');
        $form['np']['np_fields'] = array(
          '#type' => 'select',
          '#title' => t('Fields'),
          '#description' => t('Select a which fields use instead of user name.'),
          '#options' => $options,
          '#default_value' => $np_fields,
          '#multiple' => TRUE,
        );
      }
    }
    else {
      $form['types'][] = array(
        '#type' => 'item',
        '#value' => t('There are not content types marked as nodeprofile.'),
      );
    }
  }

  // Submit
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Implementation of hook_submit().
 */
function tbp_settings_form_submit($form_id, $form_values) {
  // Managed vocabularies
  $managed_vocab = array_keys(preg_grep('/^1$/', $form_values['vids']));
  tbp_set_setting('managed_vocab', $managed_vocab);

  // Allowed add-ons
  $allowed_addons = array();
  if (isset($form_values['addons'])) {
    $allowed_addons = array_keys(preg_grep('/^1$/', $form_values['addons']));
    tbp_set_setting('allowed_addons', $allowed_addons);
  }

  // Managed content types
  $managed_types = array();
  $types_cache = array();
  if (isset($form_values['types'])) {
    foreach ($form_values['types'] as $vid => $type) {
      $managed_types[$vid] = array_keys(preg_grep('/^1$/', $type));
      foreach ($managed_types[$vid] as $type) {
        $types_cache[$type] = $type;
      }
    }
    tbp_set_setting('managed_types', $managed_types);
    tbp_set_setting('types_cache', $types_cache);
  }

  // Nodeprofile
  if (isset($form_values['np']['np_type'])) {
    if ($form_values['np']['np_type'] != '') {
      tbp_set_setting('np_type', $form_values['np']['np_type']);
      if (isset($form_values['np']['np_fields'])) {
        if (is_array($form_values['np']['np_fields'])) {
          tbp_set_setting('np_fields', $form_values['np']['np_fields']);
        }
      }
    }
    else {
      variable_del('tbp_np_type');
      variable_del('tbp_np_fields');
    }
  }

  drupal_set_message(t('The changes have been saved.'));
}
